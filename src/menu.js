const menu = [
  {
    id: 1,
    type: "Breakfast",
    item1: "Garlic Bread",
    description:
      "The breaksfast has been added to the list for weekdays from Monday to Friday",
    item1URL:
      "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-1.dfe0042d.jpg",
    item2: "Veg Sandwich",
    item2URL:
      "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-3.50d620d7.jpg",
    item3: "Roast Sandwich",
    item3URL:
      "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-4.c872fffa.jpg",
  },
  {
    id: 2,
    type: "Lunch",
    item1: "Noodles",
    description:
      "The Lunch has been added to the list for weekdays from Monday to Friday",
    item1URL:
      "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-3.50d620d7.jpg",
    item2: "Veg Biriyani",
    item3: "Fried Rice",
  },
  {
    id: 3,
    type: "Dinner",
    item1: "Burger",
    description:
      "The Dinner has been added to the list for weekdays from Monday to Friday",
    item1URL:
      "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-4.c872fffa.jpg",
    item2: "Rice & Curry",
    item3: "Hoppers",
  },
];

export default menu;
