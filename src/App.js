import React from "react";
import {
  BrowserRouter as Router,
  withRouter,
  Route,
  Switch,
} from "react-router-dom";
import { ROUTE_PATHS } from "./shared/constants";
import MainLayout from "./components/MainLayout";
import RestaurantManager from "./components/RestaurantManager";
import UpdateRestaurant from "./pages/UpdateRestaurant";
import CreateRestaurant from "./pages/CreateRestaurant";
import Password from "./pages/Password";
import UserName from "./pages/UserName";
import Verification from "./pages/Verifcation";
import HomePage from "./pages/Home";
import ForgetPassword from "./pages/ForgetPassword";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={UserName} />
        <Route exact path={ROUTE_PATHS.PASSWORD} component={Password} />
        <Route exact path={ROUTE_PATHS.VERIFY} component={Verification} />
        <Route
          exact
          path={ROUTE_PATHS.FORGOTPASSWORD}
          component={ForgetPassword}
        />
        <MainLayout>
          <Route
            exact
            path={ROUTE_PATHS.MANAGER}
            component={RestaurantManager}
          />
          <Route exact path={ROUTE_PATHS.UPDATE} component={UpdateRestaurant} />
          <Route exact path={ROUTE_PATHS.CREATE} component={CreateRestaurant} />
          <Route exact path={ROUTE_PATHS.HOMEPAGE} component={HomePage} />
        </MainLayout>
      </Switch>
    </Router>
  );
};

export default App;
