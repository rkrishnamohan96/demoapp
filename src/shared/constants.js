export const ROUTE_PATHS = {
  HOME: "/home",
  MANAGER: "/restaurant-manager",
  LOGIN: "/log-in",
  UPDATE: "/update-restaurant",
  CREATE: "/create-restauarent",
  VERIFY: "/verify-login",
  PASSWORD: "/password",
  HOMEPAGE: "/homepage",
  FORGOTPASSWORD: "/forgotpassword",
};
