import { createStore } from "redux";
import counter from './reducers'

let store = createStore(counter);

store.subscribe(() => console.log(store.getState()));

store.dispatch(increment());