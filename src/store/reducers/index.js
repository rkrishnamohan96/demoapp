import counterReducer from "./counter.reducer";
import { combineReducers } from "redux";

const rootReducers = combineReducers({
    counter : counterReducer
})

export default rootReducers;