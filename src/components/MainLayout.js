/**
 * Date - 28/06/2021
 * Developer - Krishna
 */

import React from "react";
import SideMenu from "./SideMenu";
import BasicTable from "./RestaurantManager";

const MainLayout = ({ children }) => {
  return (
    <div>
      <div className="grid grid-cols-6">
        <SideMenu />
        {/* <BasicTable /> */}
        <div className="cols-span-3">{children}</div>
      </div>
    </div>
  );
};

export default MainLayout;
