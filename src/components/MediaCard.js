import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ChartTabs from "../components/ChartTabs";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 800,
    // height: 500,
    // paddingTop: -30,
    margin: theme.spacing(0),
    marginLeft: "30px",
    backgroundColor: "white",
    height: "250px",
  },
  //   media: {
  //     height: 50,
  //   },
}));

export default function MediaCard() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <ChartTabs />
      </CardContent>
    </Card>
  );
}
