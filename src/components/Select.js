import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import NativeSelect from "@material-ui/core/NativeSelect";
import ChartTabs from "../components/ChartTabs";

const useStyles = makeStyles((theme) => ({
  //   button: {
  //     display: "block",
  //     marginTop: theme.spacing(1),
  //   },
  formControl: {
    margin: theme.spacing(2),
    minWidth: 150,
    marginLeft: "30px",
  },
}));

export default function ControlledOpenSelect() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <NativeSelect
          defaultValue={10}
          inputProps={{
            name: "name",
            // id: "uncontrolled-native",
          }}
          disableUnderline
        >
          {/* <MenuItem value="">
            <em>None</em>
          </MenuItem> */}
          <option value={10}> Last 12 weeks</option>
          <option value={20}>Past 12 months</option>
        </NativeSelect>
      </FormControl>
    </div>
  );
}
