import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: 5,
    fontFamily: [
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:hover": {
      color: "black",
      opacity: 1,
    },
  },
  margin: {
    margin: theme.spacing(2),
    width: 920,
  },
  // withoutLabel: {
  //   marginTop: theme.spacing(4),
  // },
  // textField: {
  //   width: "30ch",
  // },
  textBox: {
    margin: theme.spacing(1),
    width: 300,
    paddingLeft: 9,
  },
  textField: {
    // marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
    margin: theme.spacing(1),
    width: 110,
  },
  paraText: {
    margin: theme.spacing(1),
    backgroundRepeat: "no-repeat",
    padding: 0,
    margin: 0,
  },
}));

export default function LayoutTextFields({
  name,
  description,
  location,
  owner,
  manager,
  reception,
  email,
  map,
  Latitude,
  Longtitude,
  bill,
  type,
  accountname,
  accountnumber,
  bankname,
  branch,
}) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Name</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={name}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={60}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Description
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={description}
            style={{ height: "100px" }}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={90}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Address</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={location}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={60}
          />
        </FormControl>
      </div>
      <div>
        <TextField
          error
          id="outlined-error"
          label="Mobile Number"
          className={classes.textBox}
          value={owner}
          variant="outlined"
          helperText="Please enter the Mobile number."
          labelWidth={60}
        />
        <TextField
          error
          id="outlined-error-helper-text"
          label="Office Number"
          className={classes.textBox}
          value={manager}
          helperText="Please enter the Office number."
          variant="outlined"
        />
        <TextField
          error
          id="outlined-error-helper-text"
          label="Reception"
          className={classes.textBox}
          value={reception}
          helperText="Incorrect entry."
          helperText="Please enter the recption number."
          variant="outlined"
        />
      </div>

      <div>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Email Address
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={email}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={100}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Google Map Link
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={map}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={120}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Bill</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={bill}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={30}
          />
        </FormControl>

        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Charge Type
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={type}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={100}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Charge Amount
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={bill}
            // value={map}
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={100}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Biilling Cycle
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={100}
          />
        </FormControl>
      </div>
      <div>
        <p>Bank Details</p>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Account Name
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={accountname}
            labelWidth={120}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Account Number
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={accountnumber}
            labelWidth={120}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Bank Name</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={bankname}
            labelWidth={100}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Branch Name
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={branch}
            labelWidth={100}
          />
        </FormControl>
      </div>
      <div>
        <p>Restaurant Opening Hours</p>
        <p className={classes.paraText}>Sunday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />

        <p className={classes.paraText}>Monday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Tuesday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Wednesday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Thursday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Friday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Saturday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
      </div>
    </div>
  );
}
