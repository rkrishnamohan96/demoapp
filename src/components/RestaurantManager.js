import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Typography,
  Divider,
  Grid,
  Modal,
  Backdrop,
  Fade,
  TextField,
} from "@material-ui/core";
import { Container } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import ArchiveOutlinedIcon from "@material-ui/icons/ArchiveOutlined";
import AddIcon from "@material-ui/icons/Add";
import DeleteRoundedIcon from "@material-ui/icons/DeleteRounded";
import itemArr from "../data.js";
// import CustomizedTabs from "../pages/UpdateRestaurant";
import Rating from "@material-ui/lab/Rating";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: "100px",
  },
  card: {
    width: 200,
  },
  root: {
    width: "80%",
    marginBottom: "20px",
    padding: 10,
    paddingLeft: 260,
    marginTop: -13,
  },
  table: {
    maxHeight: "100%",
    paddingLeft: "250px",
    minWidth: 650,
  },
  header: {
    backgroundColor: "orange",
  },
  edit: {
    cursor: "pointer",
    // marginLeft: "60px",
    marginRight: "60px",
  },
  typography: {
    spacing: 2,
  },
  editIcon: {
    marginRight: "10px",
  },
  popContainer: {
    padding: "40px",
  },
  heading1: {
    marginLeft: "10px",
    marginTop: 20,
  },
  addIcon: {
    height: 30,
    width: 30,
    margin: 13,
    cursor: "pointer",
  },
  addDish: {
    marginTop: 20,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    height: 500,
    width: 500,
  },
}));

export default function BasicTable(props) {
  const [item, setItem] = useState(itemArr);
  const removeItem = (id) => {
    let newItem = item.filter((item) => item.id !== id);
    setItem(newItem);
  };
  const { history } = props;
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchItem, setSerachItem] = useState("");
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [open, setOpen] = useState(false);

  // const handleOpen = () => {
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setOpen(false);
  // };

  const routeChange = () => {
    let path = `/create-restauarent`;
    history.push(path);
  };

  return (
    <>
      <Paper className={classes.root}>
        <Grid container justify="flex start">
          <TextField
            id="outlined-full-width"
            label="Search"
            style={{ margin: 8, borderColor: "orange" }}
            placeholder="Resturant name"
            fullWidth
            margin="normal"
            variant="outlined"
          />
        </Grid>
        <Grid container justify="flex-end">
          <Typography className={classes.addDish} variant="h9">
            ADD Restaurant
          </Typography>
          <AddIcon
            className={classes.addIcon}
            // onClick={handleOpen}
            onClick={routeChange}
          />

          {/* <Modal
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <h2>ADD ITEM</h2>
                <p>content</p>
              </div>
            </Fade>
          </Modal> */}
          {/* </Grid> */}
        </Grid>
        {/* <Divider></Divider> */}
        <TableContainer component={Paper}>
          <Table
            // stickyHeader
            className={classes.table}
            aria-label="simple table"
          >
            <TableHead style={{ backgroundColor: "#ff8306" }}>
              <TableRow style={{ color: "white" }}>
                <TableCell>
                  <b>ID</b>
                </TableCell>
                <TableCell>
                  <b>Name</b>
                </TableCell>
                <TableCell>
                  <b>Location</b>
                </TableCell>
                <TableCell>
                  <b>Ranking</b>
                </TableCell>
                <TableCell>
                  <b style={{ paddingLeft: "7px" }}>Rating</b>
                </TableCell>
                <TableCell>
                  <b>Edit/Archieve</b>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {item
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((rest) => (
                  <TableRow key={rest.id}>
                    <TableCell>
                      <b>{rest.id}</b>
                    </TableCell>
                    <TableCell>{rest.name}</TableCell>
                    <TableCell>{rest.location}</TableCell>
                    <TableCell>{rest.Ranking}</TableCell>
                    <TableCell>
                      <Rating value={rest.Rating} />
                    </TableCell>
                    <TableCell>
                      <EditIcon
                        className={classes.icon}
                        onClick={() =>
                          history.push(`/update-restaurant/?id=${rest.id}`, {
                            rest,
                          })
                        }
                      >
                        {" "}
                      </EditIcon>
                      <DeleteRoundedIcon
                        className={classes.icon}
                        onClick={() => removeItem(rest.id)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 20, 30]}
          component="div"
          count={itemArr.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
