/**
 * Date - 02/07/2021
 */

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import Divider from "@material-ui/core/Divider";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";

const useStyles = makeStyles({
  root: {
    maxWidth: 300,
    height: 200,
    // paddingTop: -30,
    // margin: theme.spacing(0),
    width: " 256px",
    marginLeft: "60px",
    marginBottom: "20px",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function OutlinedCard({ name }) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography>{name}</Typography>
        <Typography className={classes.pos} color="secondary">
          Needs Attention
        </Typography>
      </CardContent>
      {/* <Divider style={{ marginBottom: "50px" }} /> */}
      <Divider style={{ marginTop: "50px" }} />
      <CardActions style={{ marginTop: "5px" }}>
        <Button size="large" color="black">
          <ArrowDropUpIcon />
        </Button>
        <Button
          size="small"
          style={{ marginLeft: "60px", fontSize: "12px", color: "black" }}
        >
          view more
          <ArrowForwardIcon />
        </Button>
      </CardActions>
    </Card>
  );
}
