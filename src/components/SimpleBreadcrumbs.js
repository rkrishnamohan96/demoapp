/**
 * Date - 28/06/2021.
 * Developer - Krishna
 */

import React from "react";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { Link as RouterLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

export default function SimpleBreadcrumbs({ name, index }) {
  console.log(index);
  const history = useHistory();
  return (
    <Breadcrumbs
      separator={
        <NavigateNextIcon fontSize="small" style={{ marginLeft: "1px" }} />
      }
    >
      <Link color="inherit" onClick={() => history.push(`/restaurant-manager`)}>
        Restaurant Manager
      </Link>
      <Typography color="textPrimary">
        {index === 0
          ? name
          : index === 1
          ? "Menu"
          : index === 2
          ? "Categories"
          : index === 3
          ? "Items"
          : index === 4
          ? "Modifier Group"
          : "..."}
      </Typography>
    </Breadcrumbs>
  );
}
