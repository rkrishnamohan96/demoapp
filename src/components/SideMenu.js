import React, { useState } from "react";
import { Link } from "react-router-dom";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Button,
  Grid,
  CardMedia,
  Paper,
  ListItemIcon,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import MenuOpenIcon from "@material-ui/icons/MenuOpen";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import logo from "../assets/logo.png";
import HomeIcon from "@material-ui/icons/Home";
import PaymentIcon from "@material-ui/icons/Payment";
import RestaurantMenuIcon from "@material-ui/icons/RestaurantMenu";
import SettingsIcon from "@material-ui/icons/Settings";
import FeedbackIcon from "@material-ui/icons/Feedback";
import AccessAlarmIcon from "@material-ui/icons/AccessAlarm";
import HelpIcon from "@material-ui/icons/Help";
import grey from "@material-ui/core/colors/grey";
import { useHistory, useLocation } from "react-router-dom";

const drawerWidth = 221;
const drawColor = grey[400];
const menuItems = [
  {
    text: "Home",
    icon: <HomeIcon style={{ marginLeft: "10px" }} />,
    path: "/homepage",
  },
  {
    text: "Payments",
    icon: <PaymentIcon style={{ marginLeft: "10px" }} />,
    path: "/restaurant-manager",
  },
  {
    text: "Menu",
    icon: <RestaurantMenuIcon style={{ marginLeft: "10px" }} />,
    path: "/menu",
  },
  {
    text: "Settings",
    icon: <SettingsIcon style={{ marginLeft: "10px" }} />,
    path: "/settings",
  },
  {
    text: "Marteting",
    icon: <HomeIcon style={{ marginLeft: "10px" }} />,
    path: "/marketing",
  },
  {
    text: "Feedback",
    icon: <FeedbackIcon style={{ marginLeft: "10px" }} />,
    path: "/feedback",
  },
  {
    text: "Preparation Times",
    icon: <AccessAlarmIcon style={{ marginLeft: "10px" }} />,
    path: "/preparation-time",
  },
  {
    text: "KYC",
    icon: <HomeIcon style={{ marginLeft: "10px" }} />,
    path: "/KYC",
  },
  {
    text: "KYM",
    icon: <HomeIcon style={{ marginLeft: "10px" }} />,
    path: "/KYM",
  },
  {
    text: "Help",
    icon: <HelpIcon style={{ marginLeft: "10px" }} />,
    path: "/help",
  },
];

const useStyles = makeStyles((theme) => ({
  appBar: {
    // transition: theme.transitions.create(["margin", "width"], {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen,
    // }),
    zIndex: theme.zIndex.drawer + 1,
    height: 69,
    backgroundColor: "Orange",
  },
  // appBarShift: {
  //   width: `calc(100% - ${drawerWidth}px)`,
  //   marginLeft: drawerWidth,
  //   transition: theme.transitions.create(["margin", "width"], {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen,
  //   }),
  // },
  menuButton: {
    marginRight: theme.spacing(2),
    marginTop: 10,
    color: "white",
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    backgroundColor: "grey",
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
    marginTop: 0,
    marginBottom: 30,
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),

    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen,
    // }),
    // marginLeft: -drawerWidth,
  },
  // contentShift: {
  //   transition: theme.transitions.create("margin", {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen,
  //   }),
  //   marginLeft: 0,
  // },
  root: {
    display: "flex",
    backgroundColor: "red",
  },
  typo: {
    margin: "10px",
    marginTop: 20,
    display: "flex-start",
    color: "white",
    // fontWeight: "bold",
  },

  helpTypo: {
    margin: "8px",
    marginTop: 24,
    display: "flex-start",
    color: "white",
    // fontWeight: "bold",
  },
  img: {
    height: 50,
    marginRight: 80,
    marginTop: 10,
    borderRadius: 45,
    marginLeft: 10,
    // position: "center",
  },
  helpImg: {
    height: 50,
    marginLeft: 900,
    marginTop: 10,
    position: "center",
  },
  logout: {
    marginTop: 25,
  },
  logoText: {
    fontWeight: "bold",
  },
  content: {
    margin: 4,
  },
  active: {
    backgroundColor: "Orange",
  },
}));

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const history = useHistory();
  const location = useHistory();
  const theme = useTheme();
  // const [open, setOpen] = useState(true);
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
    // setOpen(false);
  };

  const handleDrawerClose = () => {
    // setOpen(false);
    setOpen(true);
  };

  return (
    <div className={classes.root}>
      {/* <CssBaseline /> */}
      {/* <AppBar
        position="fixed"
        // className={clsx(classes.appBar, {
        //   [classes.appBarShift]: open,
        // })}
        className={classes.appBar}
        style={{ boxShadow: "none" }}
      > */}
      {/* <Toolbar> */}
      {/* <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton> */}
      {/* <Grid container justify="space-between"> */}
      {/* <Grid>
              <div className={classes.root}>
                <Typography>
                  <img src={logo} className={classes.img} />
                </Typography>
                <Typography variant="h6" noWrap className={classes.typo}>
                  Admin Panel
                </Typography>
              </div>
            </Grid> */}
      {/* <Grid>
              <div className={classes.root}>
                <HelpOutlineIcon className={classes.helpImg} />
                <Typography variant="h7" noWrap className={classes.helpTypo}>
                  Help
                </Typography>
              </div>
            </Grid> */}
      {/* <Grid>
              <div className={classes.root}>
                <ExitToAppIcon className={classes.img} />
                <Typography variant="h7" noWrap className={classes.logout}>
                  Logout
                </Typography>
              </div>
            </Grid> */}
      {/* </Grid> */}
      {/* </Toolbar> */}
      {/* </AppBar> */}

      <Drawer
        className={classes.drawer}
        // variant="persistent"
        variant="permanent"
        anchor="left"
        // open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {/* <Toolbar /> */}
        <div className={classes.drawerHeader}>
          <img src={logo} className={classes.img} />
        </div>

        {/* list and list Items */}
        <List>
          {menuItems.map((item) => (
            <ListItem
              button
              onClick={() => history.push(item.path)}
              className={location.pathname == item.path ? classes.active : null}
              key={item.text}
            >
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      {/* <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
      </main> */}
    </div>
  );
}
