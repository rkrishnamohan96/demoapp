/**
 * Date - 23/06/2021
 * Developer - Krishna
 */

import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import LayoutTextFields from "../components/LayoutTextFields";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import SimpleBreadcrumbs from "../components/SimpleBreadcrumbs";
import Menu from "./Menu";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const AntTabs = withStyles({
  root: {
    borderBottom: "1px solid #e8e8e8",
    marginBottom: 10,
    marginTop: -10,
  },
  indicator: {
    backgroundColor: "black",
  },
})(Tabs);

const AntTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    zIndex: 200,
    fontFamily: [
      // '"Segoe UI"',
      // "Roboto",
      // '"Helvetica Neue"',
      // "Arial",
      // "sans-serif",
    ].join(","),
    "&:hover": {
      color: "black",
      opacity: 1,
    },
    "&$selected": {
      color: "black",
      fontWeight: theme.typography.fontWeightMedium,
    },
    "&:focus": {
      color: "black",
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const StyledTabs = withStyles({
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#635ee7",
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    color: "#fff",
    fontWeight: theme.typography.fontWeightRegular,
    // fontSize: theme.typography.pxToRem(15),
    marginRight: theme.spacing(1),
    "&:focus": {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
    paddingBottom: 20,
    paddingLeft: "250px",
  },
  demo2: {
    backgroundColor: "#2e1534",
  },

  // tab: {
  //   paddingTop: 50,
  //   zIndex: 1000,
  // },
  main: {
    // width: "100%",
    // zIndex: 500,
    // paddingTop: -2,
    // position: "fixed",
  },
}));

function UpdateRestaurant({ location }) {
  console.log(location.state.rest);
  const { id, name, description, type, item1, para } = location.state.rest;
  console.log(id);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <div className={classes.demo1}>
        <AntTabs
          value={value}
          onChange={handleChange}
          aria-label="ant example"
          className={classes.main}
        >
          <AntTab label="Restaurant Info" {...a11yProps(0)} />
          <AntTab label="Menus" {...a11yProps(1)} />
          <AntTab label="Categories" {...a11yProps(2)} />
          <AntTab label="Items" {...a11yProps(3)} />
          <AntTab label="Modifier Groups" {...a11yProps(4)} />
        </AntTabs>
        <TabPanel value={value} index={0} className={classes.tab}>
          <SimpleBreadcrumbs name={name} index={0} />
          <LayoutTextFields {...location.state.rest} />
        </TabPanel>
        <TabPanel value={value} index={1} className={classes.tab}>
          <SimpleBreadcrumbs index={1} />
          <Menu />
        </TabPanel>
        <TabPanel value={value} index={2} className={classes.tab}>
          <SimpleBreadcrumbs index={2} />
        </TabPanel>
        <TabPanel value={value} index={3} className={classes.tab}>
          <SimpleBreadcrumbs index={3} />
        </TabPanel>
        <TabPanel value={value} index={4} className={classes.tab}>
          <SimpleBreadcrumbs index={4} />
        </TabPanel>
      </div>
    </div>
  );
}


export default UpdateRestaurant