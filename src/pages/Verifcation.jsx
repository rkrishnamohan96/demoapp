import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles , withStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useHistory } from "react-router-dom";
import logo from "../assets/logo.png";




const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(18),
    // marginRight: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    fontFamily: "UberMoveText-Medium,Helvetica,sans-serif",
  },
  form: {
    // width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  verify: {
    margin: theme.spacing(3, 0, 2),
    width: "552px",
    height : "42px",
      backgroundColor: "#ff8f00", //#ff9800
    borderColor: "#ff8f00",
    "&:hover": {
      backgroundColor: "#ff9800",   //#ef6c00
    },
  },
  img: {
    height: 60,
    marginRight: 5,
    marginTop: 10,
    borderRadius: 45,
    position: "center",
  },

    footer: {
    // background: "#00022E",
    backgroundColor: "#262626",
    // height: "50px",
    marginTop: "90px",
    height: "78px",
    color: "white",
    position: "sticky",
    display: "flex",
  },
}));

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'Orange',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'Orange',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'Orange',
      },
      '&:hover fieldset': {
        borderColor: 'Orange',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'Orange',
      },
    },
  },
})(TextField);

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="">
        eatmeglobal
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function Verifcation() {
  const classes = useStyles();
  const history = useHistory();
  return (
    <>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
         <Typography>
          <img src={logo} className={classes.img} />
        </Typography>
        <form className={classes.form} noValidate>
          <Typography component="h1" variant="h5" style={{ marginLeft: 0 }}>
            2-step verification - SMS
          </Typography>
          <p style={{ paddingRight: "10px", marginRight: "20px" }}>
            Please enter the 4-digit code that was sent to you.
          </p>
          <CssTextField
            variant="outlined"
            margin="normal"
            required
            style={{ display: "flex", width: "552px" }}
            id="custom-css-outlined-input"
            label="Verification code"
            name="verify"
            autoComplete="verfity"
            autoFocus
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.verify}
            onClick={() => history.push(`/homepage`)}
          >
            Next
          </Button>
        </form>
      </div>
    </Container>
    <div className={classes.footer}>
        <div>
          <p
            style={{
              paddingLeft: "70px",
              fontSize: "12px",
              lineHeight: "16px",
              paddingTop: "25px",
            }}
          >
            © 2020 Eat Me global, Inc.
          </p>
        </div>
        <span style={{
              paddingLeft: "900px",
              fontSize: "12px",
              lineHeight: "30px",
              paddingTop: "25px",
            }}> Privacy Policy  |  Terms of Use</span>
      </div>
    </>
  );
}

export default Verifcation;
