/**
 * Date - 28/06/2021
 * Developer - Krishna
 */

import React from "react";
import {
  Grid,
  Paper,
  Avatar,
  TextField,
  Button,
  Typography,
  Link,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import logo from "../assets/logo.png";

const LogIn = () => {
  const paperStyle = {
    padding: 20,
    height: "75vh",
    width: 320,
    margin: "30px auto",
  };
  const avatarStyle = {
    backgroundColor: "#1bbd7e",
    // img: {
    //   height: 20,
    //   marginRight: 5,
    //   marginTop: 10,
    //   borderRadius: 45,
    //   position: "center",
    // },
  };
  const btnstyle = { margin: "8px 0", backgroundColor: "Orange" };
  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
        <Grid align="center">
          {/* <img src={logo} className={avatarStyle.img} /> */}
          <Avatar style={avatarStyle}>
            <LockOutlinedIcon />
          </Avatar>
          <h2>Log In</h2>
        </Grid>
        <TextField
          label="Username"
          placeholder="Enter username"
          fullWidth
          required
        />
        <TextField
          label="Password"
          placeholder="Enter password"
          type="password"
          fullWidth
          required
        />
        <FormControlLabel
          control={<Checkbox name="checkedB" color="primary" />}
          label="Remember me"
        />
        <Button
          type="submit"
          // color="primary"
          variant="contained"
          style={btnstyle}
          fullWidth
        >
          Log in
        </Button>
        <Typography>
          <Link href="#">Forgot password ?</Link>
        </Typography>
        <Typography>
          {" "}
          Do you have an account ?<Link href="#">Sign Up</Link>
        </Typography>
      </Paper>
    </Grid>
  );
};

export default LogIn;
