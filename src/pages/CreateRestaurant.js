/**
 * Date - 23/06/2021
 * Developer - Krishna
 */

import React, { useState } from "react";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { Link as RouterLink } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    borderBottom: "1px solid #e8e8e8",
    marginBottom: 10,
    paddingLeft: 250,
    flexWrap: "wrap",
    marginTop: -16,
    backgroundColor: "white",
  },
  margin: {
    margin: theme.spacing(2),
    width: 975,
  },

  textBox: {
    margin: theme.spacing(1),
    width: 318,
    paddingLeft: 10,
  },
  partnertextBox: {
    margin: theme.spacing(1),
    width: 240,
    paddingLeft: 10,
  },

  redemptionBox: {
    margin: theme.spacing(1),
    paddingLeft: 10,
    width: 490,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },

  line: {
    borderBottom: "5px dashed orange",
    width: "90%",
    // padding: "1rem",
  },
  textField: {
    // marginLeft: theme.spacing(1),
    marginRight: theme.spacing(2),
    // margin: theme.spacing(1),
    // width: 110,
  },
}));

const CreateRestaurant = (props) => {
  const [text, setText] = useState("");
  const { history } = props;
  const handleChange = (event) => {
    setText(event.target.value);
  };

  const handleClick = () => {
    history.push(`/restaurant-manager`);
    window.scrollTo(0, 0);
  };
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div style={{ paddingTop: "13px" }}>
        <Breadcrumbs
          separator={
            <NavigateNextIcon fontSize="small" style={{ marginLeft: "1px" }} />
          }
        >
          <Link
            color="inherit"
            onClick={() => history.push(`/restaurant-manager`)}
          >
            Restaurant Manager
          </Link>
          <Typography color="textPrimary">Create Restaurant</Typography>
        </Breadcrumbs>
        <h2>Create Restaurant</h2>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Restuarent Name
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={130}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Description
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={80}
          />
        </FormControl>
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Address</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={60}
          />
        </FormControl>
      </div>
      <div>
        <TextField
          error
          id="outlined-error"
          label="Mobile Number"
          className={classes.textBox}
          variant="outlined"
          helperText="Please enter the Mobile number."
          labelWidth={60}
        />
        <TextField
          error
          id="outlined-error-helper-text"
          label="Office Number"
          className={classes.textBox}
          helperText="Please enter the Office number."
          variant="outlined"
        />
        <TextField
          error
          id="outlined-error-helper-text"
          label="Reception"
          className={classes.textBox}
          helperText="Incorrect entry."
          helperText="Please enter the recption number."
          variant="outlined"
        />
      </div>
      <FormControl className={classes.margin} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">
          Email Address
        </InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          //   value={email}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          labelWidth={100}
        />
      </FormControl>
      <FormControl className={classes.margin} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">
          Google Map Link
        </InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          //   value={email}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          labelWidth={130}
        />
      </FormControl>
      <FormControl className={classes.margin} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">
          Direct Delivery Status
        </InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          //   value={email}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          labelWidth={160}
        />
      </FormControl>
      <FormControl className={classes.margin} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">
          Restuarent Preparation Time
        </InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          //   value={email}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          labelWidth={210}
        />
      </FormControl>
      <FormControl className={classes.margin} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">Price Range</InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          //   value={email}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          labelWidth={90}
        />
      </FormControl>
      <div>
        <h2 className={classes.line}>Partner Specific Agreement Details.</h2>
        <TextField
          id="outlined-error"
          label="Registered Merchant Name"
          className={classes.partnertextBox}
          variant="outlined"
          helperText="Registered Merchant Name is required." //Registered Merchant Name is required
          labelWidth={130}
        />
        <TextField
          id="outlined-error-helper-text"
          label="Registered Address"
          className={classes.partnertextBox}
          helperText="Registered Address is required."
          variant="outlined"
        />
        <TextField
          id="outlined-error-helper-text"
          label="Reception"
          className={classes.partnertextBox}
          helperText="Incorrect entry."
          helperText="Please enter the recption number."
          variant="outlined"
        />
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="outlined-age-native-simple">Type</InputLabel>
          <Select
            native
            value={text}
            onChange={handleChange}
            label="Age"
            inputProps={{
              name: "age",
              id: "outlined-age-native-simple",
            }}
          >
            <option>Sole Proprietorship</option>
            <option>Partnership</option>
            <option>Limited Libility Company</option>
            <option>Public Limited Company</option>
          </Select>
        </FormControl>
      </div>
      <div>
        <TextField
          id="outlined-error"
          label="Contact Person"
          className={classes.partnertextBox}
          variant="outlined"
          helperText="Contact Person is Required." //Registered Merchant Name is required
          labelWidth={130}
        />
        <TextField
          id="outlined-error-helper-text"
          label="Contact Number"
          className={classes.partnertextBox}
          helperText="Please enter a valid phone number."
          variant="outlined"
        />
      </div>
      <div>
        <h2 className={classes.line}>Redemption Details.</h2>
        <TextField
          id="outlined-error"
          label="Redeem Receipt mobile number"
          className={classes.redemptionBox}
          variant="outlined"
          helperText="Please enter a valid Mobile number."
          labelWidth={130}
        />
        <TextField
          id="outlined-error-helper-text"
          label="Redeeem Receipt Emails"
          className={classes.redemptionBox}
          helperText="Registered Address is required."
          variant="outlined"
          labelWidth={100}
        />
        <FormControl className={classes.margin} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">
            Order Sequence
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            startAdornment={<InputAdornment position="start"></InputAdornment>}
            labelWidth={130}
          />
        </FormControl>
      </div>
      <div>
        <h2 className={classes.line}>Restaurant Opening Hours.</h2>
        <p className={classes.paraText}>Sunday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />

        <p className={classes.paraText}>Monday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Tuesday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Wednesday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Thursday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Friday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <p className={classes.paraText}>Saturday</p>
        <TextField
          id="time"
          label="Open"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
        <TextField
          id="time"
          label="Close"
          type="time"
          defaultValue="07:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
      </div>
      <div style={{ paddingTop: "30px" }}>
        <Button
          variant="contained"
          onClick={handleClick}
          style={{ marginLeft: "5px", paddingTop: "10px" }}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          // onClick={handleClick}
          style={{ marginLeft: "790px", paddingTop: "10px" }}
        >
          Save
        </Button>
      </div>
    </div>
  );
};

export default CreateRestaurant;
