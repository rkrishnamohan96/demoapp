import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles , withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import logo from "../assets/logo.png";
import { useHistory } from "react-router-dom";


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="">
        eatmeglobal
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'Orange',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'Orange',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'Orange',
      },
      '&:hover fieldset': {
        borderColor: 'Orange',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'Orange',
      },
    },
  },
})(TextField);



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(18),
    // marginRight: theme.spacing(14),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    fontFamily: "UberMoveText-Medium,Helvetica,sans-serif",
  },
  form: {
    // width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width: "552px",
    height : "42px",
    backgroundColor: "#ff8f00", //#ff9800
    borderColor: "#ff8f00",
    "&:hover": {
      backgroundColor: "#ff9800",   //#ef6c00
    },
  },
  img: {
    height: 60,
    marginRight: 5,
    marginTop: 10,
    borderRadius: 45,
    position: "center",
  },

  para:{
   position : "center" , 
   marginLeft: "200px" , 
   fontSize : "16px" , 
   color : "#ff9800"
  },
    footer: {
    // background: "#00022E",
    backgroundColor: "#262626",
    // height: "50px",
    marginTop: "90px",
    height: "78px",
    color: "white",
    position: "sticky",
    display: "flex",
  },
}));

export default function ForgetPassword() {
  const classes = useStyles();
  const history = useHistory();
  return (
    <>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography>
          <img src={logo} className={classes.img} />
        </Typography>
       
        <form className={classes.form} noValidate>
        <Typography  variant="h5" style={{ marginLeft: 0 }}>
          Forgotten your Password
        </Typography>
        <p style ={{paddingRight : "10px" , marginRight:"20px"}}>
         Please enter your email
        </p>
          <CssTextField
            variant="outlined"
            type = "password"
            margin="normal"
            required
            style={{ display: "flex", width: "552px" }}
            id="custom-css-outlined-input"
            label="Password"
            name="password"
            autoComplete="user name"
            autoFocus
            placeholder = "Please enter your Email"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Next
          </Button>
        </form>
      </div>
    </Container>
    <div className={classes.footer}>
        <div>
          <p
            style={{
              paddingLeft: "70px",
              fontSize: "12px",
              lineHeight: "16px",
              paddingTop: "25px",
            }}
          >
            © 2020 Eat Me global, Inc.
          </p>
        </div>
        <span style={{
              paddingLeft: "900px",
              fontSize: "12px",
              lineHeight: "30px",
              paddingTop: "25px",
            }}> Privacy Policy  |  Terms of Use</span>
      </div>
    </>
  );
}
