import React from "react";
import {
  makeStyles,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider ,
  Typography,
  Paper,
  Grid
} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import ControlledOpenSelect from '../components/Select';
import Deposits from '../components/MissOrders'
import MediaCard from '../components/MediaCard';
import clsx from 'clsx';
import OutlinedCard from '../components/Temp'



const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  img: {
    height: 100,
    width: 150,
    marginTop: 10,
    borderRadius: 45,
    marginLeft: 30,
    marginRight: 20,
    // position: "center",
  },
}));

const Header = () => {
  const classes = useStyles();
  return (
     <Grid item xs={12} md={12} lg={12}>
      <ListItem style={{ marginRight: "100px" }}>
        <div>
          <img
            src="http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-4.c872fffa.jpg"
            className={classes.img}
          />
        </div>
        <List>
          <Typography variant="h5" style={{ marginLeft: "5px" }}>
            A&M Gampola
          </Typography>
          <ListItemIcon>
            <LocationOnOutlinedIcon 
            // style={{ paddingTop: "10px" }}
             />
            <ListItemText>Colombo-3</ListItemText>
          </ListItemIcon>
          <Typography>
            <Rating
              name="customized-empty"
              defaultValue={5}
              emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
          </Typography>
        </List>
      </ListItem>
      </Grid>
    
  );
};

const HomePage = () => {
  const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <div className={classes.root}>
         <Container maxWidth="md" >
            <Header />
            <Divider style={{width:"124%"}}/>
            <ControlledOpenSelect/>
           <Grid container 
            // direction="row"
            justify="space-between"
            alignItems="center"
            spacing={3}
           >
            <Grid item xs={9} md={9} lg={10}>   
                <MediaCard/>
            </Grid>
             <Grid item xs={3} md={3} lg={2}>
                 <OutlinedCard name={"Missed Orders"}/>
            </Grid>
            <Grid item xs={6} md={4} lg={5}>
                 <OutlinedCard name={"Top selling Items"}/>
            </Grid>
             <Grid item xs={6} md={4} lg={5}>
                 <OutlinedCard name={"Menu Item Feedback"}/>
            </Grid>
             <Grid item xs={6} md={4} lg={2}>
                 <OutlinedCard name={"In Accurate Orders"}/>
            </Grid>
           </Grid>
        </Container> 
    </div>
  );
};

export default HomePage;
