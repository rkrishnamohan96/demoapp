const ItemArr = [
  {
    id: "3",
    name: "A&M Gampola",
    location: "Kollupitiya, Colombo 3",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    owner: "07685263789",
    manager: "07685263789",
    reception: "0118523679",
    email: "abc@gmail.com",
    map: "https://www.google.com/maps/place/Gampola+Railway+Station/@7.1535723,80.5523878,13.83z/data=!4m19!1m13!4m12!1m3!2m2!1d80.5591826!2d7.123877!1m6!1m2!1s0x3ae371c740a6ba35:0x199f5ad7a0aaa198!2sGampola!2m2!1d80.564677!2d7.126777!3e0!3m4!1s0x3ae371ddef092d33:0",
    Latitude: "0.7645",
    Longtitude: "0.862",
    bill: "1000/=",
    type: "online",
    accountname: "todo",
    accountnumber: "1114556456",
    bankname: "Commercial bank Plc",
    branch: "Colombo",
    Ranking: "4",
    Rating: "1",
    // type: "Breakfast",
    // item1: "Garlic Bread",
    // para: "The breaksfast has been added to the list for weekdays from Monday to Friday",
    // item1URL:
    //   "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-1.dfe0042d.jpg",
    // item2: "Veg Sandwich",
    // item2URL:
    //   "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-3.50d620d7.jpg",
    // item3: "Roast Sandwich",
    // item3URL:
    //   "http://slidesigma.nyc/templatemonster/react/foodtech/static/media/food-4.c872fffa.jpg",
  },
  {
    id: "111",
    name: "Isso	",
    location: "Wellawatta, Colombo 6",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    Ranking: "1",
    owner: "1121815181",
    manager: "151515115151",
    reception: "44656562323",
    email: "git@gmail.com",
    Rating: "5",
  },
  {
    id: "64",
    name: "Sandwich Factory	",
    location: "Slave Island, Colombo 2",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    Ranking: "3",
    Rating: "3.5",
  },
  {
    id: "72",
    name: "Heladiv Tea Club",
    location: "Kollupitiya, Colombo 3",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    Ranking: "5",
    Rating: "4",
  },
  {
    id: "9",
    name: "PST Park Street Trattoria",
    location: "Fort, Colombo 1",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    Ranking: "1",
    Rating: "2",
  },
  {
    id: "11",
    name: "T.G.I. Friday's",
    location: "Fort, Colombo 1",
    description:
      "when customers frequent Philly Fair Trade Coffee Co., they know that their cup of coffee does more than just give them a caffeine boost. It also supports coffee farmers in Costa Rica ",
    Ranking: "2",
    Rating: "5",
  },
];

export default ItemArr;
